function distance(first, second){
	if(Array.isArray(first) && Array.isArray(second)){

		let uniqueF = [...new Set(first.filter(x => !second.includes(x)))];
		let uniqueS = [...new Set(second.filter(x => !first.includes(x)))];

		let array = uniqueF.concat(uniqueS);

		return array.length;
	}
	throw new Error('InvalidType');
}
// console.log(distance([1], ['a']));
// console.log(distance([1, 2], [1, 2]));
// console.log(distance([], []));
// console.log(distance([1], "a"));

module.exports.distance = distance